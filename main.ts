// Define this class in a separate module and use it in the main module

import { LikeComponent } from './like.component';

let component = new LikeComponent(10, true);
// Simulate the scenario where the user clicks the like component. 
component.onClick();

console.log(`likesCount: ${component.likesCount}, isSelected: ${component.isSelected}`);

// Works only if targeting ES5-->tsc *.ts --target ES5  && node main.js