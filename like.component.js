"use strict";
// Allow the consumer of this class to pass the initial 
// number of likes when creating an instance of this class. 
//new change for test
Object.defineProperty(exports, "__esModule", { value: true });
var LikeComponent = /** @class */ (function () {
    // likesCount: any;
    // isSelected: any;
    function LikeComponent(_likesCount, _isSelected) {
        this._likesCount = _likesCount;
        this._isSelected = _isSelected;
    }
    // Simulate the scenario where the user clicks the like component. 
    LikeComponent.prototype.onClick = function () {
        // Display the total number of likes
        this._likesCount += (this._isSelected) ? -1 : +1;
        // and whether the button is in the selected or unselected state on the console
        this._isSelected = !this._isSelected;
    };
    Object.defineProperty(LikeComponent.prototype, "likesCount", {
        get: function () {
            return this._likesCount;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LikeComponent.prototype, "isSelected", {
        get: function () {
            return this._isSelected;
        },
        enumerable: true,
        configurable: true
    });
    return LikeComponent;
}());
exports.LikeComponent = LikeComponent;
