// Allow the consumer of this class to pass the initial 
// number of likes when creating an instance of this class. 
//new change for test

export class LikeComponent { 
    // likesCount: any;
    // isSelected: any;
    constructor(private _likesCount: number, private _isSelected: boolean) {
    }
    // Simulate the scenario where the user clicks the like component. 
    onClick() {
        // Display the total number of likes

        this._likesCount += (this._isSelected) ? -1 : +1;
        
        // and whether the button is in the selected or unselected state on the console

        this._isSelected = !this._isSelected;
    }

    get likesCount() { 
        return this._likesCount;
    }

    get isSelected() {
        return this._isSelected;
    }
}